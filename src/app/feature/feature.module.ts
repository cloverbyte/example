import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { MaterialModule } from '../shared/material.module';
import { HelpComponent } from './components/help/help.component';



@NgModule({
  declarations: [HeaderComponent, HomeComponent, HelpComponent],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [HomeComponent, HeaderComponent, HelpComponent]
})
export class FeatureModule { }
